
function setup() {
    
    oscWebSocket = new osc.WebSocketPort({
        url: "ws://localhost:8081",
        metadata: true
    });    
    
    oscWebSocket.on("ready", onSocketOpen);
    oscWebSocket.on("message", onSocketMessage);
    oscWebSocket.open();
}


function onSocketOpen() {
    console.log("Socket open with success!");
}

function onSocketMessage(oscMsg) {
    console.log("message received", oscMsg);
}