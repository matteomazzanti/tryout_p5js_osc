# Tryout p5.js with OSC


## Install and try

1. install node_modules
```  
$ cd osc-bridge-udp-ws
$ npm install
```

2. start the "udp - websocket" server.
```  
$ node .
```

3. Send OSC messages to the "udp - websocket" server, on port 7400 on the osc 
path ```/runner```.

3. open osc-browser-ws/index.html in the browser.

4. see the OSC messanges in the browser console.

Voilà!

## Tech docs

OSC: https://github.com/colinbdclark/osc.js
